#!/usr/bin/env bash
set -euo pipefail +x

field_name=$1

grep -Eo "\"$field_name\" *: *\"(.*)\"" .sagify.json | grep -Eo ':.*".*"' | cut -d'"' -f 2
